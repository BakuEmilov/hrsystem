import React from 'react'

interface IGrayButton {
  text: string,
  className?: string,
}

const GrayButton: React.FC<IGrayButton> = ({
  text, className
}) => {
  return (
    <button className='rounded-[30px] border border-[#DDDDDD] bg-[#EFEFEF] text-[#00000080] text-[16px] font-[500] px-[26px] py-[10px] mx-2'>
      {text}
    </button>
  )
}

export default GrayButton;