import React from 'react'

interface IBlueButton {
  text: string,
  className?: string,
  onClick?: () => void,
}

const BlueButton: React.FC<IBlueButton> = ({
  text, className, onClick
}) => {
  return (
    <button className='rounded-[30px] bg-[#4A6DFF] text-white text-[16px] font-[500] px-[26px] py-[10px] mx-2' onClick={onClick}>
      {text}
    </button>
  )
}

export default BlueButton;