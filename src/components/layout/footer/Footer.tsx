import React from "react";
import Link from "next/link";

const Footer = () => {
  return (
    <div className="bg-[#F4F6FF] h-[380px]">
      <div className="w-[1200px] m-auto flex">
        <Link href="/">
          <div className="flex mr-14 mt-[70px]">
            <img className="mr-1" src="/title-icon.svg" alt="" width={55} />
            <h1 className="text-[#000041] text-[22px] font-[600]">Border</h1>
          </div>
        </Link>

        <div className="mr-auto mt-[70px] leading-[40px]">
          <h1 className="font-[600] text-[16px]">Контакты</h1>
          <p className="text-[#5C5B5B] text-[14px]">Кыргызстан, г. Бишкек</p>
          <p className="text-[#5C5B5B] text-[14px]">ул. Токтогула 112/1</p>
          <p className="text-[#5C5B5B] text-[14px]">+996 (706) 11 22 33</p>
        </div>

        <div className="flex gap-x-12 leading-[40px] mt-[70px]">
          <div className="">
            <h1 className="font-[600] text-[16px]">Соискателям</h1>
            <p className="text-[#5C5B5B] text-[14px]">Личный кабинет</p>
            <p className="text-[#5C5B5B] text-[14px]">Вакансии</p>
            <p className="text-[#5C5B5B] text-[14px]">Работодатели</p>
          </div>
          <div>
            <h1 className="font-[600] text-[16px]">Работодателям</h1>
            <p className="text-[#5C5B5B]  text-[14px]">Личный кабинет</p>
            <p className="text-[#5C5B5B] text-[14px]">Опубликовать вакансии</p>
            <p className="text-[#5C5B5B] text-[14px]">Кандидаты</p>
          </div>
          <div>
            <h1 className="font-[600] text-[18px]">Меню</h1>
            <p className="text-[#5C5B5B] text-[14px]">Вакансии</p>
            <p className="text-[#5C5B5B] text-[14px]">FAQ</p>
            <p className="text-[#5C5B5B] text-[14px]">Контакты</p>
          </div>

          <div>
            <h1 className="font-[600] text-[18px]">Поддержка</h1>
            <p className="text-[#5C5B5B] text-[14px]">
              Правила размещения вакансий
            </p>
            <p className="text-[#5C5B5B] text-[14px]">
              Политика конфиденциальности
            </p>
          </div>
        </div>
      </div>
      <div className="w-[1200px] m-auto">
        <div className="flex items-center mt-[80px]">
          <div className="ml-16 mr-auto text-[#A6A6A6] text-[14px]">
            © 2023. DevsFactory. All Right Reserved.
          </div>
          <Link href="/">
            <img className="mr-4" src="/instagram.svg" alt="" />
          </Link>
          <Link href="/">
            <img className="mr-4" src="/facebook.svg" alt="" />
          </Link>
          <Link href="/">
            <img className="mr-4" src="/youtube.svg" alt="" />
          </Link>
          <Link href="/">
            <img className="mr-4" src="/twitter.svg" alt="" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Footer;
