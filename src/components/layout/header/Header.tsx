'use client'
import React from "react";
import Link from "next/link";
import BlueButton from "@/components/ui/button/BlueButton";
import GrayButton from "@/components/ui/button/GrayButton";

const Header = () => {

  return (
    <div className="shadow-lg py-4">
      <div className="flex items-center w-[1200px] m-auto">
      <Link href="/" className="flex items-center  mr-14">
        <img className="mr-1" src="/title-icon.svg" alt="" width={55} />
        <h1 className="text-[#000041] text-[22px] font-[600]">Border</h1>
      </Link>
      <nav>
        <ul className="flex">
          <li className="text-[16px] mx-6">
            <Link href="/jobOpenings">Вакансии</Link>
          </li>
          <li className="text-[16px] mx-6">
            <Link href="/faq">FAQ</Link>
          </li>
          <li className="text-[16px] mx-6">
            <Link href="/contacts">Контакты</Link>
          </li>
        </ul>
      </nav>
      <div className="flex ml-auto">
        <GrayButton text="Войти"/>
        <BlueButton text="Регистрация"/>
      </div>
    </div>
    </div>
  );
};

export default Header;
